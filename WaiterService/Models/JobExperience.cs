﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class JobExperience : Experience
    {
        public int JobId { get; set; }
        public Job Job { get; set; }
    }
}
