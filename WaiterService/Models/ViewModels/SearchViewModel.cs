﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models.ViewModels
{
    public class SearchViewModel : Job
    {
        public List<SelectListItem> CategoriesList { get; set; }
        public List<SelectListItem> SkillsList { get; set; }
        public Job[] Jobs { get; set; }
    }
}
