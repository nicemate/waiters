﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models.ViewModels
{
    public class MatchViewModel : Job
    {
        public ProfileWithDistance[] MatchedProfiles { get; set; }
    }
}
