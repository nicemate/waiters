﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models.ViewModels
{
    public class JobViewModel : Job
    {
        public List<SelectListItem> CategoriesList { get; set; }
        public List<SelectListItem> SkillsList { get; set; }
        public List<SelectListItem> LocationsList { get; set; }
        [Required]
        public string Date { get; set; } = DateTime.Today.ToString("yyyy/MM/dd");
        [Required]
        public string Start { get; set; } = DateTime.Now.ToString("H:mm");
        [Required]
        public string End { get; set; } = DateTime.Now.ToString("H:mm");
    }
}
