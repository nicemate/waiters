﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models.ViewModels
{
    public class LocationViewModel : Location
    {
        public string Lat
        {
            get { return Latitude.ToString(); }
            set { Latitude = float.Parse(value); }
        }

        public string Long
        {
            get { return Longitude.ToString(); }
            set { Longitude = float.Parse(value); }
        }
    }
}
