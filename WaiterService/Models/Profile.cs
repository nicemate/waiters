﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public abstract class Profile
    {
        public string ProfileId { get; set; } 
        [ForeignKey(nameof(ProfileId))] 
        public ApplicationUser User { get; set; }

        [Display(Name = "Skype ID")]
        public string SkypeId { get; set; }

        public ICollection<Location> Locations { get; set; }
    }
}
