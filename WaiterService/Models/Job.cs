﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class Job
    {
        public int ID { get; set; }
        public JobExperience Experience { get; set; }
        public JobSlot Slot { get; set; }
        public string ProfileId { get; set; }
        public CompanyProfile CompanyProfile { get; set; }
        public int LocationID { get; set; }
        public Location Location { get; set; }
    }
}
