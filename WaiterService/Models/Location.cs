﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class Location
    {
        public int ID { get; set; }
         
        [Display(Name = "Location Name")]
        public string Name { get; set; }

        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public string ProfileId { get; set; }
        public Profile Profile { get; set; }
    }
}
