﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class CompanyLocation: Location
    {
        [ForeignKey(nameof(ProfileId))]
        public CompanyProfile Profile { get; set; }
    }
}
