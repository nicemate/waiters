﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class UserProfile : Profile
    {
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Date of Birth")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Current Address")]
        public string CurrentAddress { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        [Display(Name = "Whatsapp Number")]
        public string WhatsappNumber { get; set; }

        public string Gender { get; set; }

        public ICollection<UserExperience> Experiences { get; set; }
        public ICollection<UserSlot> Slots { get; set; }
        public ICollection<Education> Educations { get; set; }
    }

    public enum Gender { Male, Female }
}
