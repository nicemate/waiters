﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class UserSlot: Slot
    {
        public string ProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
