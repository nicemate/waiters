﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class CompanyProfile : Profile
    {
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Vat Number")]
        public string VatNumber { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string Facebook { get; set; }
        public ICollection<Job> Jobs { get; set; }
    }
}
