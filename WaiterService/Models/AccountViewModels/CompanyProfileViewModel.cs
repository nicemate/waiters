﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models.AccountViewModels
{
    public class CompanyProfileViewModel : CompanyProfile
    {
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }
}
