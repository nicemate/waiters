﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models.AccountViewModels
{
    public class UserProfileViewModel : UserProfile
    {
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public List<SelectListItem> GenderList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "Male", Text = "Male" },
            new SelectListItem { Value = "Female", Text = "Female" },
        };

        public static string[] Countries = {
            "Austria","Belgium","Czech Republic","Denmark",
            "Estonia","Finland","France","Germany","Greece","Hungary","Iceland ","Italy ",
            "Latvia","Liechtenstein","Lithuania","Luxembourg","Malta","Netherlands","Norway",
            "Poland","Portugal","Slovakia","Slovenia","Spain","Sweden","Switzerland"
        };

        public List<SelectListItem> CountryList { get; set; } = Countries.Select(c => new SelectListItem { Value = c, Text = c }).ToList();
    }
}
