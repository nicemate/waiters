﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using WaiterService.Models.AccountViewModels;
using Microsoft.AspNetCore.Identity;

namespace WaiterService.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public Profile Profile { get; set; }

        public ProfileType ProfileType { get; set; }
    }

    public enum ProfileType { User, Company }
}
