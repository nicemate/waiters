﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class UserExperience : Experience
    {
        public int Months { get; set; }
        public string ProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
