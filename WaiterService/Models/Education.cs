﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public class Education
    {
        public int ID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Institution { get; set; }
        public string Address { get; set; }
        [Display(Name = "Start Year")]
        [Required]
        public int StartYear { get; set; }
        [Display(Name = "End Year")]
        [Required]
        public int EndYear { get; set; }

        public string ProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
