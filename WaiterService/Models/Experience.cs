﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WaiterService.Models
{
    public abstract class Experience
    {
        public int ID { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int SkillId { get; set; }
        public Skill Skill { get; set; }
    }

    public class ExperienceMatch : Experience
    {

    }

    public static class ExperienceExtentions
    {
        public static List<ExperienceMatch> ToExperienceMatch<T> (this List<T> src)
        {
            return src.Select(e => new ExperienceMatch().Assign(e)).ToList();
        }
    }

    public class ExperienceComparer : IEqualityComparer<Experience>
    {
        public bool Equals(Experience x, Experience y)
        {
            return (x.SkillId == y.SkillId && x.CategoryId == y.CategoryId);
        }

        public int GetHashCode(Experience obj)
        {
            return new { obj.SkillId, obj.CategoryId }.GetHashCode();
        }
    }
}
