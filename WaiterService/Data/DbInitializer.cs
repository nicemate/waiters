﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaiterService.Models;

namespace WaiterService.Data
{
    public class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            // Look for any categories.
            if (context.Categories.Any())
            {
                return;   // DB has been seeded
            }

            var categories = new String[]
            {
                "Hotel", "Restaurant", "Pizza joint", "Bar", "Disco hall", "Private house", "Guest house", "Driver", "Backery", "Others"
            };

            foreach (String category in categories)
            {
                context.Categories.Add(new Category() {
                    Name = category
                });
            }
            context.SaveChanges();

            // Look for any skills.
            if (context.Skills.Any())
            {
                return;   // DB has been seeded
            }

            var skills = new String[]
            {
                "Pizzaiolo", "Cook", "Dish washer", "Barman", "Cook assistance", "Drink server", "Receptionist", "Forester", "Driver"
            };

            foreach (String skill in skills)
            {
                context.Skills.Add(new Skill()
                {
                    Name = skill
                });
            }
            context.SaveChanges();
        }
    }
}
