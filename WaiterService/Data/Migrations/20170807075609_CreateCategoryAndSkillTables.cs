﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WaiterService.Data.Migrations
{
    public partial class CreateCategoryAndSkillTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Location_JobId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_ProfileId",
                table: "Location");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndTime",
                table: "Slot",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartTime",
                table: "Slot",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "UserTempId",
                table: "Profile",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Job",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SkillId",
                table: "Job",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Skill",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skill", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Profile_UserTempId",
                table: "Profile",
                column: "UserTempId");

            migrationBuilder.CreateIndex(
                name: "IX_Location_JobId",
                table: "Location",
                column: "JobId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location_ProfileId",
                table: "Location",
                column: "ProfileId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Job_CategoryId",
                table: "Job",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Job_SkillId",
                table: "Job",
                column: "SkillId");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Category_CategoryId",
                table: "Job",
                column: "CategoryId",
                principalTable: "Category",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Skill_SkillId",
                table: "Job",
                column: "SkillId",
                principalTable: "Skill",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_Category_CategoryId",
                table: "Job");

            migrationBuilder.DropForeignKey(
                name: "FK_Job_Skill_SkillId",
                table: "Job");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "Skill");

            migrationBuilder.DropIndex(
                name: "IX_Profile_UserTempId",
                table: "Profile");

            migrationBuilder.DropIndex(
                name: "IX_Location_JobId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_ProfileId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Job_CategoryId",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Job_SkillId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "Slot");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "Slot");

            migrationBuilder.DropColumn(
                name: "UserTempId",
                table: "Profile");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "SkillId",
                table: "Job");

            migrationBuilder.CreateIndex(
                name: "IX_Location_JobId",
                table: "Location",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_Location_ProfileId",
                table: "Location",
                column: "ProfileId");
        }
    }
}
