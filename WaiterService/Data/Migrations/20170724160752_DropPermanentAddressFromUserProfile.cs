﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WaiterService.Data.Migrations
{
    public partial class DropPermanentAddressFromUserProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nationality",
                table: "Profile");

            migrationBuilder.RenameColumn(
                name: "PermanentAddress",
                table: "Profile",
                newName: "Country");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Country",
                table: "Profile",
                newName: "PermanentAddress");

            migrationBuilder.AddColumn<string>(
                name: "Nationality",
                table: "Profile",
                nullable: true);
        }
    }
}
