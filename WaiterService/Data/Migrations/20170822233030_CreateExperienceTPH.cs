﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WaiterService.Data.Migrations
{
    public partial class CreateExperienceTPH : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_Category_CategoryId",
                table: "Job");

            migrationBuilder.DropForeignKey(
                name: "FK_Job_Skill_SkillId",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Profile_UserTempId",
                table: "Profile");

            migrationBuilder.DropIndex(
                name: "IX_Job_CategoryId",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Job_SkillId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "UserTempId",
                table: "Profile");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "SkillId",
                table: "Job");

            migrationBuilder.CreateTable(
                name: "Experience",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryId = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    SkillId = table.Column<int>(nullable: false),
                    JobId = table.Column<int>(nullable: true),
                    Months = table.Column<int>(nullable: true),
                    ProfileId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Experience", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Experience_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Experience_Skill_SkillId",
                        column: x => x.SkillId,
                        principalTable: "Skill",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Experience_Job_JobId",
                        column: x => x.JobId,
                        principalTable: "Job",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Experience_Profile_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "ProfileId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Profile_ProfileId",
                table: "Profile",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Experience_CategoryId",
                table: "Experience",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Experience_SkillId",
                table: "Experience",
                column: "SkillId");

            migrationBuilder.CreateIndex(
                name: "IX_Experience_JobId",
                table: "Experience",
                column: "JobId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Experience_ProfileId",
                table: "Experience",
                column: "ProfileId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Experience");

            migrationBuilder.DropIndex(
                name: "IX_Profile_ProfileId",
                table: "Profile");

            migrationBuilder.AddColumn<int>(
                name: "UserTempId",
                table: "Profile",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Job",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SkillId",
                table: "Job",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Profile_UserTempId",
                table: "Profile",
                column: "UserTempId");

            migrationBuilder.CreateIndex(
                name: "IX_Job_CategoryId",
                table: "Job",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Job_SkillId",
                table: "Job",
                column: "SkillId");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Category_CategoryId",
                table: "Job",
                column: "CategoryId",
                principalTable: "Category",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Skill_SkillId",
                table: "Job",
                column: "SkillId",
                principalTable: "Skill",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
