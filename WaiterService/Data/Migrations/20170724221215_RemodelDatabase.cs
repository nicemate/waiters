﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WaiterService.Data.Migrations
{
    public partial class RemodelDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profile_Location_LocationID",
                table: "Profile");

            migrationBuilder.DropTable(
                name: "LocationUserProfile");

            migrationBuilder.DropIndex(
                name: "IX_Profile_LocationID",
                table: "Profile");

            migrationBuilder.DropColumn(
                name: "LocationID",
                table: "Profile");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Location",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<float>(
                name: "Latitude",
                table: "Location",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "Longitude",
                table: "Location",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ZipCode",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JobId",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProfileId",
                table: "Location",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Job",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProfileId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Job", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Job_Profile_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "ProfileId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Slot",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    JobId = table.Column<int>(nullable: true),
                    ProfileId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Slot", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Slot_Job_JobId",
                        column: x => x.JobId,
                        principalTable: "Job",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Slot_Profile_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "ProfileId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Location_JobId",
                table: "Location",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_Location_ProfileId",
                table: "Location",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Job_ProfileId",
                table: "Job",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Slot_JobId",
                table: "Slot",
                column: "JobId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Slot_ProfileId",
                table: "Slot",
                column: "ProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Job_JobId",
                table: "Location",
                column: "JobId",
                principalTable: "Job",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Profile_ProfileId",
                table: "Location",
                column: "ProfileId",
                principalTable: "Profile",
                principalColumn: "ProfileId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Job_JobId",
                table: "Location");

            migrationBuilder.DropForeignKey(
                name: "FK_Location_Profile_ProfileId",
                table: "Location");

            migrationBuilder.DropTable(
                name: "Slot");

            migrationBuilder.DropTable(
                name: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Location_JobId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_ProfileId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "ZipCode",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "JobId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "ProfileId",
                table: "Location");

            migrationBuilder.AddColumn<int>(
                name: "LocationID",
                table: "Profile",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LocationUserProfile",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LocationId = table.Column<int>(nullable: false),
                    ProfileId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationUserProfile", x => x.ID);
                    table.ForeignKey(
                        name: "FK_LocationUserProfile_Location_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LocationUserProfile_Profile_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "ProfileId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Profile_LocationID",
                table: "Profile",
                column: "LocationID");

            migrationBuilder.CreateIndex(
                name: "IX_LocationUserProfile_LocationId",
                table: "LocationUserProfile",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_LocationUserProfile_ProfileId",
                table: "LocationUserProfile",
                column: "ProfileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profile_Location_LocationID",
                table: "Profile",
                column: "LocationID",
                principalTable: "Location",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
