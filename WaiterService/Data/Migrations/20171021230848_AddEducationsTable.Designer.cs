﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WaiterService.Data;
using WaiterService.Models;

namespace WaiterService.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20171021230848_AddEducationsTable")]
    partial class AddEducationsTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("WaiterService.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<int>("ProfileType");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("WaiterService.Models.Category", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Category");
                });

            modelBuilder.Entity("WaiterService.Models.Education", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int>("EndYear");

                    b.Property<string>("Institution");

                    b.Property<string>("ProfileId");

                    b.Property<int>("StartYear");

                    b.Property<string>("Title");

                    b.HasKey("ID");

                    b.HasIndex("ProfileId");

                    b.ToTable("Education");
                });

            modelBuilder.Entity("WaiterService.Models.Experience", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryId");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<int>("SkillId");

                    b.HasKey("ID");

                    b.HasIndex("CategoryId");

                    b.HasIndex("SkillId");

                    b.ToTable("Experience");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Experience");
                });

            modelBuilder.Entity("WaiterService.Models.Job", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("LocationID");

                    b.Property<string>("ProfileId");

                    b.HasKey("ID");

                    b.HasIndex("LocationID");

                    b.HasIndex("ProfileId");

                    b.ToTable("Job");
                });

            modelBuilder.Entity("WaiterService.Models.Location", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City");

                    b.Property<string>("Country");

                    b.Property<float>("Latitude");

                    b.Property<float>("Longitude");

                    b.Property<string>("Name");

                    b.Property<string>("ProfileId");

                    b.Property<string>("State");

                    b.Property<string>("ZipCode");

                    b.HasKey("ID");

                    b.HasIndex("ProfileId");

                    b.ToTable("Location");
                });

            modelBuilder.Entity("WaiterService.Models.Profile", b =>
                {
                    b.Property<string>("ProfileId");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("SkypeId");

                    b.HasKey("ProfileId");

                    b.ToTable("Profile");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Profile");
                });

            modelBuilder.Entity("WaiterService.Models.Skill", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Skill");
                });

            modelBuilder.Entity("WaiterService.Models.Slot", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<DateTime>("EndTime");

                    b.Property<DateTime>("StartTime");

                    b.HasKey("ID");

                    b.ToTable("Slot");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Slot");
                });

            modelBuilder.Entity("WaiterService.Models.JobExperience", b =>
                {
                    b.HasBaseType("WaiterService.Models.Experience");

                    b.Property<int>("JobId");

                    b.HasIndex("JobId")
                        .IsUnique();

                    b.ToTable("JobExperience");

                    b.HasDiscriminator().HasValue("JobExperience");
                });

            modelBuilder.Entity("WaiterService.Models.UserExperience", b =>
                {
                    b.HasBaseType("WaiterService.Models.Experience");

                    b.Property<int>("Months");

                    b.Property<string>("ProfileId");

                    b.HasIndex("ProfileId");

                    b.ToTable("ProfileExperience");

                    b.HasDiscriminator().HasValue("UserExperience");
                });

            modelBuilder.Entity("WaiterService.Models.CompanyProfile", b =>
                {
                    b.HasBaseType("WaiterService.Models.Profile");

                    b.Property<string>("Address");

                    b.Property<string>("CompanyName");

                    b.Property<string>("Facebook");

                    b.Property<string>("VatNumber");

                    b.Property<string>("Website");

                    b.ToTable("CompanyProfile");

                    b.HasDiscriminator().HasValue("CompanyProfile");
                });

            modelBuilder.Entity("WaiterService.Models.UserProfile", b =>
                {
                    b.HasBaseType("WaiterService.Models.Profile");

                    b.Property<string>("City");

                    b.Property<string>("Country");

                    b.Property<string>("CurrentAddress");

                    b.Property<string>("DateOfBirth");

                    b.Property<string>("FirstName");

                    b.Property<string>("Gender");

                    b.Property<string>("LastName");

                    b.Property<string>("WhatsappNumber");

                    b.ToTable("UserProfile");

                    b.HasDiscriminator().HasValue("UserProfile");
                });

            modelBuilder.Entity("WaiterService.Models.JobSlot", b =>
                {
                    b.HasBaseType("WaiterService.Models.Slot");

                    b.Property<int>("JobId");

                    b.HasIndex("JobId")
                        .IsUnique();

                    b.ToTable("JobSlot");

                    b.HasDiscriminator().HasValue("JobSlot");
                });

            modelBuilder.Entity("WaiterService.Models.UserSlot", b =>
                {
                    b.HasBaseType("WaiterService.Models.Slot");

                    b.Property<string>("ProfileId");

                    b.HasIndex("ProfileId");

                    b.ToTable("UserSlot");

                    b.HasDiscriminator().HasValue("UserSlot");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("WaiterService.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("WaiterService.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WaiterService.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WaiterService.Models.Education", b =>
                {
                    b.HasOne("WaiterService.Models.UserProfile", "UserProfile")
                        .WithMany()
                        .HasForeignKey("ProfileId");
                });

            modelBuilder.Entity("WaiterService.Models.Experience", b =>
                {
                    b.HasOne("WaiterService.Models.Category", "Category")
                        .WithMany("Experiences")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WaiterService.Models.Skill", "Skill")
                        .WithMany("Experiences")
                        .HasForeignKey("SkillId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WaiterService.Models.Job", b =>
                {
                    b.HasOne("WaiterService.Models.Location", "Location")
                        .WithMany()
                        .HasForeignKey("LocationID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WaiterService.Models.CompanyProfile", "CompanyProfile")
                        .WithMany("Jobs")
                        .HasForeignKey("ProfileId");
                });

            modelBuilder.Entity("WaiterService.Models.Location", b =>
                {
                    b.HasOne("WaiterService.Models.Profile", "Profile")
                        .WithMany("Locations")
                        .HasForeignKey("ProfileId");
                });

            modelBuilder.Entity("WaiterService.Models.Profile", b =>
                {
                    b.HasOne("WaiterService.Models.ApplicationUser", "User")
                        .WithOne("Profile")
                        .HasForeignKey("WaiterService.Models.Profile", "ProfileId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WaiterService.Models.JobExperience", b =>
                {
                    b.HasOne("WaiterService.Models.Job", "Job")
                        .WithOne("Experience")
                        .HasForeignKey("WaiterService.Models.JobExperience", "JobId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WaiterService.Models.UserExperience", b =>
                {
                    b.HasOne("WaiterService.Models.UserProfile", "UserProfile")
                        .WithMany("Experiences")
                        .HasForeignKey("ProfileId");
                });

            modelBuilder.Entity("WaiterService.Models.JobSlot", b =>
                {
                    b.HasOne("WaiterService.Models.Job", "Job")
                        .WithOne("Slot")
                        .HasForeignKey("WaiterService.Models.JobSlot", "JobId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("WaiterService.Models.UserSlot", b =>
                {
                    b.HasOne("WaiterService.Models.UserProfile", "UserProfile")
                        .WithMany("Slots")
                        .HasForeignKey("ProfileId");
                });
        }
    }
}
