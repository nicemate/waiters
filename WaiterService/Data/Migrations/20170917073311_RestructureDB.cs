﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WaiterService.Data.Migrations
{
    public partial class RestructureDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Job_JobId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Profile_ProfileId",
                table: "Profile");

            migrationBuilder.DropIndex(
                name: "IX_Location_JobId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_ProfileId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "JobId",
                table: "Location");

            migrationBuilder.AddColumn<int>(
                name: "LocationID",
                table: "Job",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Location_ProfileId",
                table: "Location",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Job_LocationID",
                table: "Job",
                column: "LocationID");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Location_LocationID",
                table: "Job",
                column: "LocationID",
                principalTable: "Location",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_Location_LocationID",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Location_ProfileId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Job_LocationID",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "LocationID",
                table: "Job");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Location",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "JobId",
                table: "Location",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profile_ProfileId",
                table: "Profile",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Location_JobId",
                table: "Location",
                column: "JobId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location_ProfileId",
                table: "Location",
                column: "ProfileId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Job_JobId",
                table: "Location",
                column: "JobId",
                principalTable: "Job",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
