﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WaiterService.Models;

namespace WaiterService.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        // User TPH
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<CompanyProfile> CompanyProfiles { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        
        // Slot TPH
        public DbSet<UserSlot> UserSlots { get; set; }
        public DbSet<JobSlot> JobSlots { get; set; }
        public DbSet<Slot> Slots { get; set; }

        // Experience TPH
        public DbSet<UserExperience> UserExperiences { get; set; }
        public DbSet<JobExperience> JobExperiences { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        
        public DbSet<Location> Locations { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Education> Educations { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<UserProfile>().ToTable("UserProfile");
            builder.Entity<CompanyProfile>().ToTable("CompanyProfile");
            builder.Entity<Profile>().ToTable("Profile");
            
            builder.Entity<UserSlot>().ToTable("UserSlot");
            builder.Entity<JobSlot>().ToTable("JobSlot");
            builder.Entity<Slot>().ToTable("Slot");

            builder.Entity<UserExperience>().ToTable("ProfileExperience");
            builder.Entity<JobExperience>().ToTable("JobExperience");
            builder.Entity<Experience>().ToTable("Experience");

            builder.Entity<Location>().ToTable("Location");
            builder.Entity<Category>().ToTable("Category");
            builder.Entity<Skill>().ToTable("Skill");
            builder.Entity<Job>().ToTable("Job");
            builder.Entity<Education>().ToTable("Education");
        }
    }
}
