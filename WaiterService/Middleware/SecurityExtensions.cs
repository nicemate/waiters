﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using WaiterService.Models;

namespace WaiterService.Middleware
{
    public static class SecurityExtensions
    {
        public static string GetUserId(this IIdentity identity)
        {
            var id = identity as ClaimsIdentity;
            var query = from claims in id.Claims
                        where claims.Type == ClaimTypes.NameIdentifier
                        select claims.Value;
            return query.FirstOrDefault();
        }

        public static ApplicationUser GetUser(this IIdentity identity)
        {
            var claims = identity as ClaimsIdentity;
            return GetUser(claims.Claims.ToArray());
        }

        public static ApplicationUser GetUser(this Claim[] claims)
        {
            string GetClaim(string type) => claims.Where(c => c.Type == type).Select(c => c.Value).FirstOrDefault();

            return new ApplicationUser
            {
                Email = GetClaim(ClaimTypes.Email),
                Id = GetClaim(ClaimTypes.NameIdentifier),
            };
        }
    }
}
