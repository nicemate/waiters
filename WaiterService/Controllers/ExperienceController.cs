﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WaiterService.Models;
using WaiterService.Models.AccountViewModels;
using WaiterService.Services;
using WaiterService.Middleware;
using WaiterService.Data;
using WaiterService.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace WaiterService.Controllers
{
    [Authorize(Roles = "User")]
    public class ExperienceController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly ApplicationDbContext _context;

        public ExperienceController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        
        //GET: /Experience/View
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();

            var query = from experiences in _context.Set<UserExperience>()
                        .Include(e => e.Category)
                        .Include(e => e.Skill)
                        where experiences.ProfileId == userId
                        select experiences;

            var model = await query.ToArrayAsync();

            return View(model);
        }

        //GET: /Experience/Create
        [HttpGet]
        public IActionResult Create()
        {
            var model = new ExperienceViewModel();

            var categories = _context.Categories.ToList();
            var skills = _context.Skills.ToList();
            model.CategoriesList = categories.Select(c => new SelectListItem()
            {
                Text = c.Name,
                Value = Convert.ToString(c.ID)
            }).ToList();
            model.SkillsList = skills.Select(s => new SelectListItem()
            {
                Text = s.Name,
                Value = Convert.ToString(s.ID)
            }).ToList();

            return View(model);
        }

        //POST: /Experience/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExperienceViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.ProfileId = User.Identity.GetUserId();
                var experience = new UserExperience().Assign(model);
                
                _context.UserExperiences.Add(experience);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            var categories = _context.Categories.ToList();
            var skills = _context.Skills.ToList();

            model.CategoriesList = categories.Select(c => new SelectListItem()
            {
                Text = c.Name,
                Value = Convert.ToString(c.ID)
            }).ToList();
            model.SkillsList = skills.Select(s => new SelectListItem()
            {
                Text = s.Name,
                Value = Convert.ToString(s.ID)
            }).ToList();

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //GET: /Experience/Edit
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();

            var query = from experience in _context.Set<UserExperience>()
                        .Include(e => e.Category)
                        .Include(e => e.Skill)
                        where experience.ProfileId == userId
                        where experience.ID == id
                        select experience;

            var record = await query.FirstOrDefaultAsync();

            if (record == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var model = new ExperienceViewModel().Assign(record);

            var categories = _context.Categories.ToList();
            var skills = _context.Skills.ToList();
            model.CategoriesList = categories.Select(c => new SelectListItem()
            {
                Text = c.Name,
                Value = Convert.ToString(c.ID)
            }).ToList();
            model.SkillsList = skills.Select(s => new SelectListItem()
            {
                Text = s.Name,
                Value = Convert.ToString(s.ID)
            }).ToList();

            return View(model);
        }


        //POST: /Experience/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ExperienceViewModel model)
        {
            var userId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                var experience = _context.UserExperiences
                                .Where(e => e.ProfileId == userId)
                                .FirstOrDefault(e => e.ID == model.ID);

                experience.Assign(model);
                _context.UserExperiences.Update(experience);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            var categories = _context.Categories.ToList();
            var skills = _context.Skills.ToList();

            model.CategoriesList = categories.Select(c => new SelectListItem()
            {
                Text = c.Name,
                Value = Convert.ToString(c.ID)
            }).ToList();
            model.SkillsList = skills.Select(s => new SelectListItem()
            {
                Text = s.Name,
                Value = Convert.ToString(s.ID)
            }).ToList();

            // If we got this far, something failed, redisplay form
            return View(model);
        }
    }
}
