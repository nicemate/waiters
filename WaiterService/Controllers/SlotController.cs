﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WaiterService.Models;
using WaiterService.Models.AccountViewModels;
using WaiterService.Services;
using WaiterService.Middleware;
using WaiterService.Data;
using WaiterService.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace WaiterService.Controllers
{
    [Authorize]
    public class SlotController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly ApplicationDbContext _context;

        public SlotController(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        //GET: /Job/Index
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();

            var query = from slots in _context.Set<UserSlot>()
                        where slots.ProfileId == userId
                        select new SlotViewModel
                        {
                            id = slots.ID,
                            title = $"slot {slots.ID}",
                            start = slots.StartTime,
                            end = slots.EndTime,
                        };

            var model = await query.ToArrayAsync();

            return View(model);
        }

        //POST: /Job/Create
        [HttpPost]
        public async Task<JsonResult> Create(SlotViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();

                var slot = new UserSlot
                {
                    StartTime = model.start,
                    EndTime = model.end,
                    ProfileId = userId
                };

                var result = _context.UserSlots.Add(slot);
                await _context.SaveChangesAsync();

                model.id = result.Entity.ID;
            }

            return Json(model);
        }

        //POST: /Job/Create
        [HttpPost]
        public async Task<JsonResult> Update(SlotViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();

                var query = from slot in _context.Set<UserSlot>()
                            where slot.ID == model.id
                            where slot.ProfileId == userId
                            select slot;

                var record = await query.FirstOrDefaultAsync();

                if (record != null)
                {
                    record.StartTime = model.start;
                    record.EndTime = model.end;

                    _context.UserSlots.Update(record);
                    await _context.SaveChangesAsync();

                    return Json(model);
                }

                return Json(null);
            }

            return Json(model);
        }

        //POST: /Job/Create
        public async Task<JsonResult> Delete(int id)
        {
            var userId = User.Identity.GetUserId();

            var query = from slot in _context.Set<UserSlot>()
                        where slot.ID == id
                        where slot.ProfileId == userId
                        select slot;

            var record = await query.FirstOrDefaultAsync();

            if (record != null)
            {
                var result = _context.UserSlots.Remove(record);
                await _context.SaveChangesAsync();

                return Json(result.Entity);
            }

            return Json(null);
        }
    }
}
