﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WaiterService.Models;
using WaiterService.Models.AccountViewModels;
using WaiterService.Services;
using WaiterService.Middleware;
using WaiterService.Data;
using Microsoft.EntityFrameworkCore;

namespace WaiterService.Controllers
{
    [Authorize]
    public class EducationController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly ApplicationDbContext _context;

        public EducationController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        //GET: /Education/View
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();

            var query = from locations in _context.Set<Education>()
                        where locations.ProfileId == userId
                        select locations;

            var model = await query.ToArrayAsync();

            return View(model);
        }

        //GET: /Education/Create
        [HttpGet]
        public IActionResult Create()
        {
            var model = new Education();
            return View(model);
        }

        //POST: /Education/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Education model)
        {
            if (!ModelState.IsValid)
            {
                // If we got this far, something failed, redisplay form
                return View(model);
            }

            model.ProfileId = User.Identity.GetUserId();

            _context.Educations.Add(new Education().Assign(model));
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        //GET: /Education/Edit
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();

            var query = from location in _context.Set<Education>()
                        where location.ProfileId == userId
                        where location.ID == id
                        select location;

            var model = await query.FirstOrDefaultAsync();

            if (model == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new Education().Assign(model));
        }


        //POST: /Education/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Education model)
        {
            var userId = User.Identity.GetUserId();

            if (!ModelState.IsValid)
            {
                // If we got this far, something failed, redisplay form
                return View(model);
            }

            var education = _context.Educations
                            .Where(l => l.ProfileId == userId)
                            .FirstOrDefault(e => e.ID == model.ID);

            education.Assign(model);
            _context.Educations.Update(education);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
