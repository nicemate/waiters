﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WaiterService.Models;
using WaiterService.Models.AccountViewModels;
using WaiterService.Services;
using WaiterService.Middleware;
using WaiterService.Data;
using Microsoft.EntityFrameworkCore;
using WaiterService.Models.ViewModels;

namespace WaiterService.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly ApplicationDbContext _context;

        public ProfileController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        //GET: /Account/CompanyProfile
        [HttpGet]
        public async Task<IActionResult> CompanyProfile()
        {
            var model = new CompanyProfileViewModel();
            model.ProfileId = User.Identity.GetUserId();

            var profile = _context.CompanyProfiles.FirstOrDefault(p => p.ProfileId == model.ProfileId);
            model.Assign(profile);

            var user = await _userManager.GetUserAsync(User);
            // Add PhoneNumber
            model.PhoneNumber = user.PhoneNumber;

            return View(model);
        }

        //
        // POST: /Account/CompanyProfile
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CompanyProfile(CompanyProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var profile = _context.CompanyProfiles.FirstOrDefault(p => p.ProfileId == model.ProfileId);
                if (profile == null)
                {
                    _context.Add(new CompanyProfile().Assign(model));
                }
                else
                {
                    profile.Assign(model);
                    _context.Update(profile);
                }

                var userId = User.Identity.GetUserId();
                var user = await _userManager.GetUserAsync(User);
                // Add PhoneNumber
                user.PhoneNumber = model.PhoneNumber;
                await _userManager.UpdateAsync(user);

                await _context.SaveChangesAsync();
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        //GET: /Account/UserProfileDetails
        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {
            var model = new UserProfileViewModel();

            if (String.IsNullOrEmpty(id))
            {
               id = User.Identity.GetUserId();
            }

            model.ProfileId = id;

            var profile = _context.UserProfiles.Include(p => p.Educations).FirstOrDefault(p => p.ProfileId == model.ProfileId);
            model.Assign(profile);

            var user = await _userManager.GetUserAsync(User);
            // Add PhoneNumber
            model.PhoneNumber = user.PhoneNumber;

            return View(model);
        }


        //GET: /Account/UserProfile
        [HttpGet]
        public async Task<IActionResult> UserProfile()
        {
            var model = new UserProfileViewModel();
            model.ProfileId = User.Identity.GetUserId();

            var profile = _context.UserProfiles.FirstOrDefault(p => p.ProfileId == model.ProfileId);
            model.Assign(profile);

            var user = await _userManager.GetUserAsync(User);
            // Add PhoneNumber
            model.PhoneNumber = user.PhoneNumber;

            return View(model);
        }

        //
        // POST: /Account/UserProfile
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UserProfile(UserProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var profile = _context.UserProfiles.FirstOrDefault(p => p.ProfileId == model.ProfileId);
                if (profile == null)
                {
                    _context.Add(new UserProfile().Assign(model));
                }
                else
                {
                    profile.Assign(model);
                    _context.Update(profile);
                }

                var userId = User.Identity.GetUserId();
                var user = await _userManager.GetUserAsync(User);
                // Add PhoneNumber
                user.PhoneNumber = model.PhoneNumber;
                await _userManager.UpdateAsync(user);

                await _context.SaveChangesAsync();
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //GET: /Profile/Location
        [HttpGet]
        public IActionResult Location()
        {
            var userId = User.Identity.GetUserId();

            var model = _context.Locations.FirstOrDefault(l => l.ProfileId == userId);

            return View(new LocationViewModel().Assign(model));
        }

        //POST: /Profile/Location
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Location(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                
                var location = _context.Locations.FirstOrDefault(l => l.ProfileId == userId);
                if (location == null)
                {
                    model.ProfileId = userId;                    
                    _context.Add(new Location().Assign(model));
                }
                else
                {
                    location.Assign(model, new string[] { nameof(model.ID) });
                    location.ProfileId = userId;
                    _context.Update(location);
                }
                await _context.SaveChangesAsync();
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
    }
}
