﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WaiterService.Models;
using WaiterService.Models.AccountViewModels;
using WaiterService.Services;
using WaiterService.Middleware;
using WaiterService.Data;
using WaiterService.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using Itenso.TimePeriod;
using GeoCoordinatePortable;

namespace WaiterService.Controllers
{
    [Authorize(Roles = "Company")]
    public class JobController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly ApplicationDbContext _context;

        public JobController(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        //GET: /Job/Index
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();

            var query = from jobs in _context.Set<Job>()
                        .Include(j => j.Experience.Category)
                        .Include(j => j.Experience.Skill)
                        .Include(j => j.Slot)
                        .Include(j => j.Location)
                        .Include(j => j.CompanyProfile)
                        where jobs.ProfileId == userId
                        select jobs;

            var model = await query.ToArrayAsync();

            return View(model);
        }


        //GET: /Job/Create
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var userId = User.Identity.GetUserId();

            var model = await new JobViewModel().AttachSelectLists(_context, userId);            
            // If location doesn't exist
            if (model.LocationsList.Count < 1)
            {
                return RedirectToAction("Create", "Location");
            }

            return View(model);
        }

        //POST: /Job/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(JobViewModel model)
        {
            model.ProfileId = User.Identity.GetUserId();
            if (!ModelState.IsValid)
            {
                // If we got this far, something failed, redisplay form
                return View(await model.AttachSelectLists(_context, model.ProfileId));
            }
            
            var job = new Job().Assign(model);

            // Start and End Date
            var start = DateTime.Parse($"{model.Date} {model.Start}");
            var end = DateTime.Parse($"{model.Date} {model.End}");

            job.Slot = new JobSlot
            {
                StartTime = start,
                EndTime = end
            };

            _context.Jobs.Add(job);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }


        [AllowAnonymous]
        //GET: /Job/Details
        public async Task<IActionResult> Details(int Id)
        {
            var userId = User.Identity.GetUserId();

            var query = from jobs in _context.Set<Job>()
                        .Include(j => j.Experience.Category)
                        .Include(j => j.Experience.Skill)
                        .Include(j => j.Slot)
                        .Include(j => j.Location)
                        .Include(j => j.CompanyProfile)
                        where jobs.ID == Id
                        select jobs;

            var model = await query.FirstOrDefaultAsync();

            if (model == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }


        //GET: /Job/Edit
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();

            var query = from job in _context.Set<Job>()
                        .Include(j => j.Experience)
                        .Include(j => j.Slot)
                        .Include(j => j.Location)
                        where job.ProfileId == userId
                        where job.ID == id
                        select job;

            var record = await query.FirstOrDefaultAsync();

            if (record == null)
            {
                return RedirectToAction("Index", "Home");
            }

            var model = new JobViewModel().Assign(record);
            model.Date = record.Slot.StartTime.ToString("yyyy/MM/dd");
            model.Start = record.Slot.StartTime.ToString("H:mm:ss");
            model.End = record.Slot.EndTime.ToString("H:mm:ss");

            return View(await model.AttachSelectLists(_context, userId));
        }

        //POST: /Job/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(JobViewModel model)
        {
            var userId = User.Identity.GetUserId();

            if (!ModelState.IsValid)
            {
                // If we got this far, something failed, redisplay form
                return View(await model.AttachSelectLists(_context, userId));
            }

            var job = _context.Jobs
                    .Include(j => j.Slot)
                        .Include(j => j.Location)
                            .Include(j => j.Experience)
                            .Where(j => j.ProfileId == userId)
                                .FirstOrDefault(j => j.ID == model.ID);

            // Start and End Date
            job.Slot.StartTime = DateTime.Parse($"{model.Date} {model.Start}");
            job.Slot.EndTime = DateTime.Parse($"{model.Date} {model.End}");

            job.LocationID = model.LocationID;
            job.Experience = model.Experience;

            _context.Jobs.Update(job);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
        

        [AllowAnonymous]
        //GET: /Job/Match
        [HttpGet]
        public async Task<IActionResult> Match(int Id)
        {
            var userId = User.Identity.GetUserId();

            var query = from jobs in _context.Set<Job>()
                        .Include(j => j.Experience.Category)
                        .Include(j => j.Experience.Skill)
                        .Include(j => j.Slot)
                        .Include(j => j.Location)
                        .Include(j => j.CompanyProfile)
                        where jobs.ID == Id
                        // Only current and future jobs
                        where jobs.Slot.EndTime > DateTime.Now
                        select jobs;

            var job = await query.FirstOrDefaultAsync();
           
            if (job == null)
            {
                return RedirectToAction("Index", "Job");
            }

            var jobPeriod = new TimeRange(job.Slot.StartTime, job.Slot.EndTime);
            var jCoord = new GeoCoordinate(job.Location.Latitude, job.Location.Longitude);

            var profiles = await _context.UserProfiles
                .Include(p => p.Locations)
                .Include(p => p.Experiences)
                .Include(p => p.Slots)
                .ToListAsync();

            var query_ = from profile in profiles

                         where profile.Experiences.Any(e => (e.SkillId == job.Experience.SkillId && e.CategoryId == job.Experience.CategoryId))
                         where profile.Slots.Any(s => new TimeRange(s.StartTime, s.EndTime, false).HasInside(jobPeriod))

                         where profile.Locations.Any()
                         let distance = new GeoCoordinate(profile.Locations.FirstOrDefault().Latitude, profile.Locations.FirstOrDefault().Longitude).GetDistanceTo(jCoord)
                         // Only distance less than 50 KM
                         where distance <= 50000
                         orderby distance ascending

                         select new ProfileWithDistance {
                             ProfileId = profile.ProfileId,
                             Distance = distance,
                             FirstName = profile.FirstName,
                             LastName = profile.LastName,
                             Location = profile.Locations.FirstOrDefault()
                         };

            var records = query_.ToArray();

            var model = new MatchViewModel().Assign(job);
            model.MatchedProfiles = records;

            return View(model);
        }

    }

    public static class JobExtension {
        public static async Task<JobViewModel> AttachSelectLists(this JobViewModel model, ApplicationDbContext _context, string userId)
        {
            model.CategoriesList = await _context.Categories
                .Select(c => new SelectListItem()
                {
                    Text = c.Name,
                    Value = Convert.ToString(c.ID)
                }).ToListAsync();
            model.SkillsList = await _context.Skills
                .Select(s => new SelectListItem()
                {
                    Text = s.Name,
                    Value = Convert.ToString(s.ID)
                }).ToListAsync();

            model.LocationsList = await _context.Locations.Where(l => l.ProfileId == userId)
                .Select(l => new SelectListItem()
                {
                    Text = l.Name,
                    Value = Convert.ToString(l.ID)
                }).ToListAsync();

            return model;
        }
    }
}
