﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WaiterService.Models;
using WaiterService.Models.AccountViewModels;
using WaiterService.Services;
using WaiterService.Middleware;
using WaiterService.Data;
using WaiterService.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using Itenso.TimePeriod;

namespace WaiterService.Controllers
{
    public class SearchController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly ApplicationDbContext _context;

        public SearchController(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        //GET: /Search/Index
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            // If authenticated user, match experience
            if (User.Identity.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId();
                // Get profile slots
                var pSlots = await _context.UserSlots
                                .Where(s => s.ProfileId == userId)
                                .ToListAsync();
                // Get job slots
                var jSlots = await _context.JobSlots
                                // Only current and future jobs
                                .Where(s => s.EndTime > DateTime.Now)
                                .ToListAsync();
                // Find slot matches
                var sMatchIds = jSlots.
                               Where(j => pSlots.Any(
                                   p => new TimeRange(p.StartTime, p.EndTime).HasInside(new TimeRange(j.StartTime, j.EndTime))
                               ))
                               .Select(s => s.ID).ToList();
                //return Json(sMatchIds);

                // Get profile experiences
                var pExperiences = await _context.UserExperiences
                                .Where(e => e.ProfileId == userId)
                                .ToListAsync();
                // Get job experiences
                var jExperiences = await _context.JobExperiences
                                .ToListAsync();
                // Find experience matches
                var eMatchIds = jExperiences.Intersect(pExperiences, new ExperienceComparer()).Select(e => e.ID).ToList();
                //return Json(eMatchIds);

                var temp = from jobs in _context.Set<Job>()
                           .Include(j => j.Experience.Category)
                           .Include(j => j.Experience.Skill)
                           .Include(j => j.Slot)
                           .Include(j => j.Location)
                           .Include(j => j.CompanyProfile)
                           where eMatchIds.Contains(jobs.Experience.ID) && sMatchIds.Contains(jobs.Slot.ID)
                           orderby jobs.ID descending
                           select jobs;

                return View(await temp.Take(6).ToArrayAsync());
            }

            var query = from jobs in _context.Set<Job>()
                        .Include(j => j.Experience.Category)
                        .Include(j => j.Experience.Skill)
                        .Include(j => j.Slot)
                        .Include(j => j.Location)
                        .Include(j => j.CompanyProfile)
                        orderby jobs.ID descending
                        select jobs;

            var model = await query.Take(6).ToArrayAsync();

            return View(model);
        }

        
        //GET: /Search/Query
        [HttpGet]
        public IActionResult Query()
        {
            var model = new JobViewModel();
            model.Location = new Location();
            
            var categories = _context.Categories.ToList();
            var skills = _context.Skills.ToList();
            model.CategoriesList = categories.Select(c => new SelectListItem()
            {
                Text = c.Name,
                Value = Convert.ToString(c.ID)
            }).ToList();
            model.SkillsList = skills.Select(s => new SelectListItem()
            {
                Text = s.Name,
                Value = Convert.ToString(s.ID)
            }).ToList();

            return View(model);
        }
    }
}
