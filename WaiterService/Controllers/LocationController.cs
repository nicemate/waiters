﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WaiterService.Models;
using WaiterService.Models.AccountViewModels;
using WaiterService.Services;
using WaiterService.Middleware;
using WaiterService.Data;
using WaiterService.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace WaiterService.Controllers
{
    [Authorize(Roles = "Company")]
    public class LocationController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly ApplicationDbContext _context;

        public LocationController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        
        //GET: /Location/View
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var userId = User.Identity.GetUserId();

            var query = from locations in _context.Set<Location>()
                        where locations.ProfileId == userId
                        select locations;

            var model = await query.ToArrayAsync();

            return View(model);
        }

        //GET: /Location/Create
        [HttpGet]
        public IActionResult Create()
        {
            var model = new LocationViewModel();
            return View(model);
        }

        //POST: /Location/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(LocationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                // If we got this far, something failed, redisplay form
                return View(model);
            }

            model.ProfileId = User.Identity.GetUserId();

            _context.Locations.Add(new Location().Assign(model));
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        //GET: /Location/Edit
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            var userId = User.Identity.GetUserId();

            var query = from location in _context.Set<Location>()
                        where location.ProfileId == userId
                        where location.ID == id
                        select location;

            var model = await query.FirstOrDefaultAsync();

            if (model == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(new LocationViewModel().Assign(model));
        }


        //POST: /Location/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(LocationViewModel model)
        {
            var userId = User.Identity.GetUserId();

            if (!ModelState.IsValid)
            {
                // If we got this far, something failed, redisplay form
                return View(model);
            }

            var location = _context.Locations
                            .Where(l => l.ProfileId == userId)
                            .FirstOrDefault(e => e.ID == model.ID);

            location.Assign(model);
            _context.Locations.Update(location);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
